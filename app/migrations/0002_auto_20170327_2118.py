# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-03-28 04:18
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='eventmodel',
            old_name='allDay',
            new_name='all_day',
        ),
        migrations.RenameField(
            model_name='eventmodel',
            old_name='backgroundColor',
            new_name='background_color',
        ),
        migrations.RenameField(
            model_name='eventmodel',
            old_name='borderColor',
            new_name='border_color',
        ),
        migrations.RenameField(
            model_name='eventmodel',
            old_name='textColor',
            new_name='text_color',
        ),
    ]

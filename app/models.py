from django.db import models
from django.contrib.auth.models import User
import datetime


class Event(models.Model):
    owner = models.ForeignKey(User, null=True, blank=True, )
    title = models.CharField(max_length=100)
    image = models.ImageField(null=True, blank=True, upload_to='images')
    all_day = models.BooleanField()
    start = models.DateTimeField(default=datetime.date.today)
    end = models.DateTimeField(default=datetime.date.today)

    def image_thumbnail(self):
        if self.image:
            return '<img src="/media/%s" width="100" height="65" />' % self.image
        return "No Image"

    image_thumbnail.allow_tags = True

    def __str__(self):
        return str(self.image)

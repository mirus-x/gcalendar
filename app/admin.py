from django.contrib import admin
from django.contrib.admin import ModelAdmin

from app.models import Event


class EventAdmin(ModelAdmin):
    list_display = ('image_thumbnail', 'title', 'owner', 'start', 'end', 'all_day')
    readonly_fields = ('owner', 'image_thumbnail',)

    def save_model(self, request, instance, form, change):
        user = request.user
        instance = form.save(commit=False)
        if not change or not instance.owner:
            instance.owner = user
        instance.modified_by = user
        instance.save()
        form.save_m2m()
        return instance

    def get_queryset(self, request):
        qs = super(EventAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(owner=request.user)


admin.site.register(Event, EventAdmin)

import datetime

from django.http import HttpResponseRedirect
from django.shortcuts import render, render_to_response
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic.edit import FormView
from app.models import Event
from app.form import EventForm


class HomeView(FormView):
    template_name = 'home.html'
    form_class = EventForm
    success_url = '/'

    @method_decorator(login_required())
    def get(self, request, *args, **kwargs):

        today = datetime.datetime.now()

        events = Event.objects.filter(start__month=today.month, owner=request.user).\
            values("title", "all_day", "start", "end", "pk", "image")

        data = []

        for event in events:
            data.append({"title": event['title'],
                         "allDay": str(event['all_day']).lower(),
                         "start": event['start'].isoformat(),
                         "end": event['end'].isoformat(),
                         "event_id": event['pk'],
                         "image": event['image']
                         })

        return render(request, self.template_name, {'user': request.user, "form": self.form_class,  "events": data})

    @method_decorator(login_required())
    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, request.FILES)
        if form.is_valid():
            calendar = form.save(commit=False)
            calendar.owner = request.user
            calendar.save()
            return HttpResponseRedirect('/')
        else:
            return render_to_response(self.template_name, {'form': form})